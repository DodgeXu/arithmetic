import java.lang.reflect.Array;
import java.util.ArrayList;

public class merged_sort {

    public static void main(String[] args){
        System.out.println("******************Sort************************");

        int[] A = {11,8,3,9,7,1,2,5};
        System.out.println("Origin Array:" + common.printArray(A));

        // Merged Sort
        merged_sort(A,A.length);
        System.out.println("Merged Sorted Array:" + common.printArray(A));

    }



    static int[] merged_sort(int[] array,int length){
        int[] temp = new int[length];

        merge_sort_c(array,0,length-1);

        return temp;
    }

    static void merge_sort_c(int[] array, int p,int r){

        // 终止条件
        if (p >= r) return;

        int q = (r + p)/2;

        //拆分为两个部分
        merge_sort_c(array,p,q);
        merge_sort_c(array,q+1,r);

        //合并两个部分
        merge(array,p,q,r);

    }

    // 合并两部分，先进行比较排序然后进行合并
    static void merge(int[] array, int p,int q,int r){
        int[] temp = new int[r - p + 1];

        int i = p;
        int j = q + 1;
        int k = 0;

        // 循环进行比较
        while (i<=q && j<=r){

            if (array[i] <= array[j]){
                temp[k] = array[i];
                i++;
            }else {
                temp[k] = array[j];
                j++;
            }
            k++;
        }

        // 默认是前面部分还有空余，然后判断后面部分数组是否还没到尾部
        int start = i,end = q;
        if (j <= r){
            start = j;
            end = r;
        }

        // copy 剩余的部分到temp数组中
        for (int m = start;m <=end;m++){
            temp[k] = array[m];
            k++;
        }

        // temp数组copy到原数组中的要拆分的位置
        for (int n = 0; n <= r - p; n++){
            array[p+n] = temp[n];
        }
    }


}
